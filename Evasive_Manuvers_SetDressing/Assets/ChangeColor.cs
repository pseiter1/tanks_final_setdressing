﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    public List<Renderer> renderers;

    public List<Color> colors;

    int currentColor = 0;

    //public ParticleSystem shellExplosion;

    Renderer rend;
    void Start()
    {
        rend = GetComponent<Renderer>();

        /*if (colors.Count > 0)
        {
            Color c = colors[Random.Range(0, colors.Count)];
        }*/
    }

    void Update()
    {
        if (Input.GetButtonUp("Fire3"))
        {
            currentColor++;

            //ParticleSystem.MainModule psMain = shellExplosion.main;
            //shellExplosion.Emit(currentColor);

            if (currentColor == colors.Count)
            {
                currentColor = 0;
                
            }

            // SetColor(currentColor);
        }
    }

    public Color GetCurrentColor()
    {
        return colors[currentColor];
    }

    public void SetColor(int c)
    {
        for (int i = 0; i < renderers.Count; i++)
        {
            renderers[i].material.SetColor("_Color", colors[c]);
        }
    }

}
